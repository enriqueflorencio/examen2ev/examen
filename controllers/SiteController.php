<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Maillot;
use app\models\Etapa;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    public function actionConsulta2(){
        /*$total = Yii::$app -> db
                -> createCommand('select 1')
                -> queryScalar();*/
        $consulta = new SqlDataProvider ([
          'sql' => "SELECT DISTINCT c.dorsal , c.nombre FROM etapa 
                          INNER JOIN ciclista c USING(dorsal)
                          WHERE c.dorsal NOT IN (SELECT ciclista.dorsal FROM ciclista 
                                                    INNER JOIN lleva l USING(dorsal)
                                                    INNER JOIN maillot m USING(código)
                                                    WHERE m.color = 'rosa')",
          //'totalCount' => $total,
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['dorsal','nombre'],
          "titulo" => "Consulta 2 Examen",
          "explicacion" => "Primero he averiguado cuales son los ciclistas que han ganado "
            . "etapas (SELECT DISTINCT c.dorsal FROM etapa INNER JOIN ciclista c USING(dorsal))"
            . "luego, mediante la union de las tablas ciclista, lleva y maillos, he averiguado"
            . "los ciclistas que tienen el maillot rosa"
            . "(SELECT ciclista.dorsal FROM ciclista INNER JOIN lleva l USING(dorsal)INNER JOIN maillot m USING(código) WHERE m.color = 'rosa')"
            . "Por último he 'unido' dichas consultas mediante Where c.dorsal not in (la consulta anterior) y asi he sacado los dorsales"
            . "de los ciclistas que no han ganado maillot rosa y lo he metido a la primera consulta"
            
        ]);
    }
    public function actionConsulta3(){
         $consulta = new ActiveDataProvider ([
          'query' => Etapa::find()-> select("SUM(kms) as totalKm"),
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['totalKm'],
          "titulo" => "Consulta 3 examen",
          "explicacion" => "Para realizar esta consultar, he entendido que es el "
            . "número total de km de todas las etapas, por eso he utilizado la "
            . "función SUM()",
        ]);
    }
}
